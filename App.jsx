import React, { Fragment } from 'react';

import { MapCssModules } from '@utils';
import { Header, Footer, ScrollToTop } from '@components';
import { HashRouter as Router } from "react-router-dom";

import Wrapper from './src/Wrapper/Wrapper.jsx';

const App = () => (
  <Router>
    <ScrollToTop>
      <Header />
      <Wrapper />
      <Footer />
    </ScrollToTop>
  </Router>
);

export default MapCssModules(App);
