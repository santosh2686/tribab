import React from 'react';
import { Button, Text, Row, Col, Image } from '@base';

import productData from '@data/product.json'

const ProductDetail = ({ match: {
    params: { category, id }
} }) => {
    const {
        productList, description = [], features = [], sizes = []
    } = productData[category]
    const product = productList.filter(product => {
        return product.code === id
    })[0]
    const { largeImageUrl, title } = product
    return (
        <Row>
            <Col col={{ xs: 12, sm: 12, mg: 4, lg: 4 }} classes="pad-b-20">
                <Image src={largeImageUrl} name={title} classes="width-100 bor-radius-5" />
            </Col>
            <Col col={{ xs: 12, sm: 12, mg: 8, lg: 8 }} classes="pad-b-20 pad-lg-l-30">
                <Text tag="h3" color="primary-dark" classes="pad-b-10">
                    {title}
                </Text>
                <Text tag="h4" classes="pad-t-5 pad-b-5">
                    Product description:
                </Text>
                {description.map((descriptionData, index) => {
                    const key = `description_${id}_${index}`
                    return (
                        <Text
                            key={key}
                            tag="p"
                            color="gray"
                            classes="pad-b-15"
                        >
                            {descriptionData}
                        </Text>
                    )
                })}
                <Text tag="h4" classes="pad-b-5">
                    Features:
                </Text>
                {features.map((feature, index) => {
                    const key = `feature_${id}_${index}`
                    return (
                        <Text tag="p" color="gray" key={key}>
                            {index + 1}. {feature}
                        </Text>
                    )
                })}
                <Text tag="h4" classes="pad-t-15 pad-b-5">
                    Size:
                </Text>
                {sizes.map((item, index) => {
                    const { size, code } = item
                    const key = `size_${id}_${index}`
                    return (
                        <Button key={key}
                            outline={code !== id}
                            asLink
                            href={`/product-detail/${category}/${code}`}
                            classes="mar-r-10 mar-b-10"
                        >
                            {size}
                        </Button>
                    )
                })}
            </Col>
        </Row>
    )
}

export default ProductDetail
