import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Anchor, Row, Col, Text, Layout } from '@base';

import ContactUs from './ContactUs.jsx';
import Faq from './Faq.jsx';
import WhereToBuy from './WhereToBuy.jsx';

const ContactMaster = ({
    match: { url }
}) => (
        <Row>
            <Col col={{ xs: 12, sm: 12, md: 3, lg: 2 }} classes="pad-b-20">
                <Text tag="div" size="18" weight="semi-bold" classes="bor-b-gray-light pad-b-10">
                    Contact Us Topics
                </Text>
                <Layout flex={{ direction: 'column' }}>
                    <Anchor
                        asLink
                        href="/contact/detail"
                        noUnderline
                        color="gray"
                        hoverColor="theme"
                        classes="pad-tb-10 bor-b-gray-light"
                        attributes={{
                            activeClassName: 'active-menu',
                        }}
                    >
                        Contact Us
                    </Anchor>
                    <Anchor
                        asLink
                        href="/contact/faq"
                        noUnderline
                        color="gray"
                        hoverColor="theme"
                        classes="pad-tb-10 bor-b-gray-light"
                        attributes={{
                            activeClassName: 'active-menu',
                        }}
                    >
                        FAQ's
                    </Anchor>
                    <Anchor
                        asLink
                        href="/contact/where"
                        noUnderline
                        color="gray"
                        hoverColor="theme"
                        classes="pad-tb-10 bor-b-gray-light"
                        attributes={{
                            activeClassName: 'active-menu',
                        }}
                    >
                        Where to buy
                    </Anchor>
                </Layout>
            </Col>
            <Col col={{ xs: 12, sm: 12, md: 9, lg: 10 }} classes="pad-b-20">
                <Switch>
                    <Route exact path={url} render={() => <Redirect to={`${url}/detail`} />} />
                    <Route path={`${url}/detail`} component={ContactUs} />
                    <Route path={`${url}/faq`} component={Faq} />
                    <Route path={`${url}/where`} component={WhereToBuy} />
                    <Route render={() => <Redirect to={`${url}/detail`} />} />
                </Switch>
            </Col>
        </Row>
    )
export default ContactMaster
