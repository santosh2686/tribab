import React from 'react';
import { Button, Row, Col, TextArea, TextInput, Text, Layout } from '@base';

const ContactUs = () => (
    <Layout pad={{ lg: { l: 20, r: 20 }, md: { l: 20, r: 20 } }}>
        <h3>Contact Us</h3>
        <Text tag="p" align="justify" classes="pad-t-15">
            Thank you for visiting the Tribab® website - we’d love to hear from you.
        </Text>
        <Text tag="p" align="justify" classes="pad-tb-15">
            To ensure that your questions and/or requests are addressed as quickly as possible,
            please use the form below and we will get back you as soon as possible:
        </Text>
        <form>
            <Row>
                <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="pad-b-20">
                    <TextInput
                        label="Name"
                        required
                    />
                </Col>
                <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="pad-b-20">
                    <TextInput
                        label="Email"
                        required
                    />
                </Col>
            </Row>
            <Row>
                <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="pad-b-20">
                    <TextInput
                        label="Contact"
                        required
                    />
                </Col>
                <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="pad-b-20">
                    <TextInput
                        label="Subject"
                        required
                    />
                </Col>
            </Row>
            <div>
                <TextArea
                    label="Message"
                />
            </div>
            <Layout flex={{ justify: 'end' }} pad={{ t: 15 }} classes="flex-md-down-column">
                <Button type="submit" category="theme">
                    Submit
                </Button>
            </Layout>
        </form>
    </Layout>
)
export default ContactUs
