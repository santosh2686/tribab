import React from 'react';
import { Text, Layout } from '@base';

const WhereToBuy = () => (
    <Layout pad={{ lg: { l: 20, r: 20 }, md: { l: 20, r: 20 } }}>
        <h3>Where To Buy</h3>
        <Text tag="p" align="justify" classes="pad-t-15">
            You can buy our products from below location.
        </Text>
        <Layout pad={{ t: 20 }}>
            <Text color="black">1. HIRANYAJAY ENTERPRISES PRIVATE LIMITED FACTORY</Text>
            <Text tag="p" size="12" classes="pad-l-15">(PAWNE MIDC,NAVI MUMBAI)</Text>
        </Layout>
        <Layout pad={{ t: 20 }}>
            <Text color="black">2. VIGHNAHAR ENTERPRISES</Text>
            <Text tag="p" size="12" classes="pad-l-15">(PAWNE MIDC,NAVI MUMBAI)</Text>
        </Layout>
        <Layout pad={{ t: 20 }}>
            <Text color="black">3. CHAUDHARI ENTERPRISES</Text>
            <Text tag="p" size="12" classes="pad-l-15">(PAWNE MIDC,NAVI MUMBAI)</Text>
        </Layout>
    </Layout>
)
export default WhereToBuy
