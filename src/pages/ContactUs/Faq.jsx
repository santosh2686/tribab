import React from 'react';
import { Text, Layout } from '@base';

const Faq = () => (
    <Layout pad={{ lg: { l: 20, r: 20 }, md: { l: 20, r: 20 } }}>
        <h3>Frequently asked questions</h3>
        <Layout border={{ b: "gray-light" }} pad={{ tb: 20 }}>
            <Text tag="p" weight="semi-bold" size="18" color="primary-dark" classes="pad-b-5">
                What are the types of products does your company manufacturing?
            </Text>
            <Text tag="p">
                We deals in household products like dish cleaner, hand cleaner,
                toilet cleaner, floor cleaner, green super strong phenyl,
                milky phenyl, liquid bleach and also deal in washing powder.
            </Text>
        </Layout>
        <Layout border={{ b: "gray-light" }} pad={{ tb: 15 }}>
            <Text tag="p" weight="semi-bold" size="18" color="primary-dark" classes="pad-b-5">
                Your products are ph balance?
            </Text>
            <Text tag="p">
                Yes, we maintain ph.
            </Text>
        </Layout>
        <Layout border={{ b: "gray-light" }} pad={{ tb: 15 }}>
            <Text tag="p" weight="semi-bold" size="18" color="primary-dark" classes="pad-b-5">
                Which perfume do you use for products?
            </Text>
            <Text tag="p" classes="pad-b-5">
                Here is the list of perfumes we used:
                </Text>
            <p>
                1. HAND CLEANER – GREEN APPLE,
                </p>
            <p>
                2. DISH CLEANER – LIME FLAVOUR,
                </p>
            <p>
                3. FLOOR CLEANER- JASMINE, GREEN SUPER STRONG – PINE FRAGRANCE,
                </p>
            <p>
                4. MILKY WHITE PHENYL – PINE AND CITRONELLA.
                </p>
        </Layout>
        <Layout border={{ b: "gray-light" }} pad={{ tb: 15 }}>
            <Text tag="p" weight="semi-bold" size="18" color="primary-dark" classes="pad-b-5">
                Where is your factory and godown situated?
            </Text>
            <Text tag="p">
                Both factory and godown are situated at Pawne village, ghansoli, navi Mumbai.
            </Text>
        </Layout>
        <Layout border={{ b: "gray-light" }} pad={{ tb: 15 }}>
            <Text tag="p" weight="semi-bold" size="18" color="primary-dark" classes="pad-b-5">
                How much time will it take to deliver the products?
            </Text>
            <Text tag="p">
                We deliver within 4 to 5 days after your confirmation.
            </Text>
        </Layout>
        <Layout pad={{ tb: 15 }}>
            <Text tag="p" weight="semi-bold" size="18" color="primary-dark" classes="pad-b-5">
                What are the ways to make a payment?
            </Text>
            <Text tag="p">
                We accept the payment by RTGS, NEFT, IMPS or cheque.
            </Text>
        </Layout>
    </Layout>
)
export default Faq
