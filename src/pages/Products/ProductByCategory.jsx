import React from 'react';
import { Layout, LayoutItem } from '@base';
import { ProductModule } from '@components';

import productData from '@data/product.json'

const ProductByCategory = ({ match: { params } }) => {
    const { category } = params
    const { heading, productList } = (productData[category])
    return (
        <Layout pad={{ lg: { l: 20, r: 20 }, md: { l: 20, r: 20 } }}>
            <h3>{heading}</h3>
            <Layout flex={{ align: 'center', wrap: true }} classes="flex-md-down-unset">
                {productList.map((product, index) => {
                    const { code, title, imageUrl } = product
                    return (
                        <LayoutItem classes="mar-t-20 text-center pad-md-r-25 pad-lg-r-25" key={index}>
                            <ProductModule
                                productCategory={category}
                                imageUrl={imageUrl}
                                productName={title}
                                productId={code}
                            />
                        </LayoutItem>
                    )
                })}
            </Layout>
        </Layout>
    )
}

export default ProductByCategory
