import React, { Fragment } from 'react';
import { Text, Layout, LayoutItem } from '@base';
import { ProductModule } from '@components';
import productData from '@data/product.json';

import SectionHeader from './SectionHeader.jsx';


const AllProducts = () => (
    <Layout pad={{ lg: { l: 20, r: 20 }, md: { l: 20, r: 20 } }}>
        <h3>All Products</h3>
        <Text tag="p" align="justify" classes="pad-t-15">
            Tribab removes tough stains efficiently, in whichever format that best
            suits your needs. From Detergent powder to hand wash, our product will
            help you remove even the most stubborn stains and give you hygienic cleaning,
            while Tribab products are especially great for heavily stained surface and cloth,
            providing you with a quick and effective clean. Find the right Tribab product for you.
        </Text>
        {Object.keys(productData).map((key) => {
            const { heading, productList } = productData[key]
            return (
                <Fragment key={heading}>
                    <SectionHeader
                        title={heading}
                        route={`/product/${key}`}
                    />
                    <Layout flex={{ align: 'center', wrap: true }} classes="flex-md-down-unset">
                        {productList.map((product, index) => {
                            const { code, title, imageUrl } = product
                            return (
                                <LayoutItem classes="mar-t-20 text-center pad-md-r-25 pad-lg-r-25" key={index}>
                                    <ProductModule
                                        productCategory={key}
                                        imageUrl={imageUrl}
                                        productName={title}
                                        productId={code}
                                    />
                                </LayoutItem>
                            )
                        })}
                    </Layout>
                </Fragment>
            )
        })}
    </Layout>
)
export default AllProducts
