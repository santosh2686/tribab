import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Anchor, Row, Col, Text, Layout } from '@base';

import productData from '@data/product.json';

import AllProducts from './AllProducts.jsx';
import ProductByCategory from './ProductByCategory.jsx';

const ProductMaster = ({
    match: { url }
}) => (
        <Row>
            <Col col={{ xs: 12, sm: 12, md: 3, lg: 2 }} classes="pad-b-20">
                <Text tag="div" size="18" weight="semi-bold" classes="bor-b-gray-light pad-b-10">
                    Product Categories
        </Text>
                <Layout flex={{ direction: 'column' }}>
                    <Anchor
                        asLink
                        href="/product/all"
                        noUnderline
                        color="gray"
                        hoverColor="theme"
                        classes="pad-tb-10 bor-b-gray-light"
                        attributes={{
                            activeClassName: 'active-menu',
                        }}
                    >
                        All Products
                    </Anchor>
                    {Object.keys(productData).map((key) => {
                        const { heading } = productData[key]
                        return (
                            <Anchor
                                key={key}
                                asLink
                                href={`/product/${key}`}
                                noUnderline
                                color="gray"
                                hoverColor="theme"
                                classes="pad-tb-10 bor-b-gray-light"
                                attributes={{
                                    activeClassName: 'active-menu',
                                }}
                            >
                                {heading}
                            </Anchor>
                        )
                    })}
                </Layout>
            </Col>
            <Col col={{ xs: 12, sm: 12, md: 9, lg: 10 }} classes="pad-b-20">
                <Switch>
                    <Route exact path={url} render={() => <Redirect to={`${url}/all`} />} />
                    <Route path={`${url}/all`} component={AllProducts} />
                    <Route path={`${url}/:category`} component={ProductByCategory} />
                    <Route render={() => <Redirect to={`${url}/all`} />} />
                </Switch>
            </Col>
        </Row>
    )
export default ProductMaster
