import React from 'react';
import { string } from 'prop-types'
import { Layout, Anchor } from '@base';

const SectionHeader = ({ title, route }) => (
    <Layout
        flex={{ align: 'center', justify: 'space-between' }}
        pad={{ tb: 5, lr: 10 }}
        mar={{ t: 20 }}
        bgColor="gray-lighter"
    >
        <h4>{title}</h4>
        <Anchor
            asLink
            href={route}
            noUnderline
        >
            View All
        </Anchor>
    </Layout>
)

SectionHeader.propTypes = {
    title: string,
    route: string
}

SectionHeader.defaultProps = {
    title: '',
    route: ''
}

export default SectionHeader
