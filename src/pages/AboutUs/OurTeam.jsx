import React from 'react';
import { MapCssModules } from '@utils';
import { Text, Layout, Row, Col, Image } from '@base';
import profile from '../../images/profile.png'

const OurTeam = () => (
    <Layout pad={{ lg: { l: 20, r: 20 }, md: { l: 20, r: 20 } }}>
        <h3>Our Team: Key to Success</h3>
        <Text tag="p" align="justify" classes="pad-t-15">
            For being a successful company it is very necessary to have best team which works to manufacture the best product and helps in both attaining client satisfaction and growth of the company. Same is the case with the success of our company, it is the team of our experts that had really worked hard and helped us in reaching the heights of success in a shorter duration of time.
        </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            We know the importance of best employees in an organization that is why, we conduct different rounds of interviews to short-list excellent candidates for our firm.
        </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            Here are the characteristics of our team of professionals:
        </Text>
        <Layout tag="ul" pad={{ tb: 20 }}>
            <li styleName="pad-b-10">1. They are highly qualified and well trained.</li>
            <li styleName="pad-b-10">2. Dedicated to deliver the orders in a timely manner.</li>
            <li styleName="pad-b-10">3. Bring innovative ideas to reduce the cost per unit of the manufactured product.</li>
            <li styleName="pad-b-10">4. Stay updated regarding the needs of the customers in the market.</li>
        </Layout>
        <Row>
            <Col col={{ xs: 12, sm: 12, md: 4, lg: 4}} classes="text-center pad-t-20">
                <Image src={profile} name="ABHISHEK KAWADE, Managing Director" classes="profile-image" />
                <Text tag="div" color="black" weight="semi-bold">ABHISHEK KAWADE</Text>
                <Text tag="span">Managing Director</Text>
            </Col>
            <Col col={{ xs: 12, sm: 12, md: 4, lg: 4}} classes="text-center pad-t-20">
                <Image src={profile} name="AJAY MANSUKH, Managing Director" classes="profile-image" />
                <Text tag="div" color="black" weight="semi-bold">AJAY MANSUKH</Text>
                <Text tag="span">Managing Director</Text>
            </Col>
            <Col col={{ xs: 12, sm: 12, md: 4, lg: 4}} classes="text-center pad-t-20">
                <Image src={profile} name="PRATIRAJ SHINDE, Managing Director" classes="profile-image" />
                <Text tag="div" color="black" weight="semi-bold">PRATIRAJ SHINDE</Text>
                <Text tag="span">Managing Director</Text>
            </Col>
        </Row>
    </Layout>
)

export default MapCssModules(OurTeam)
