import React from 'react';
import { Text, Layout, Row, Col } from '@base';
const companyFeatures = [{
    label: 'Nature of Business',
    value: 'Supplier, Manufacturer and Trader'
},
{
    label: 'Year of Establishment',
    value: '2018'
},
{
    label: 'No. of Employees',
    value: '05'
},
{
    label: 'Monthly Production Capacity',
    value: 'AS Per Clients Order'
},
{
    label: 'Warehousing Facility',
    value: 'Yes'
},
{
    label: 'Banker',
    value: 'IDBI Bank'
},
{
    label: 'Minimum Order Quantity',
    value: '500 KG'
},
{
    label: 'Niche Market',
    value: 'India'
},
{
    label: 'No. of Production Units',
    value: '01'
},
{
    label: 'Production Type',
    value: 'Automatic and Semi-Automatic'
},
{
    label: 'GST No.',
    value: '27AAECH5675B1ZM'
}]
const CompanyProfile = () => (
    <Layout pad={{ lg: { l: 20, r: 20 }, md: { l: 20, r: 20 } }}>
        <h3>Company Profile</h3>
        <Text tag="p" align="justify" classes="pad-t-15">
            <strong>Hiranyajay Enterprises Private Limited</strong> is a leading firm that deals with
            a wide range of products like Detergent Powder, Handwash, Green Phenyl, Black Phenyl, etc. We utilize the ingredients of superior quality in the production process of our items that makes these best in performance and effectiveness.
        </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            Our company was started recently in the year 2018 and is based in Navi Mumbai, Maharashtra, we have supplied our products to a good number of clients in lesser duration of time. The professionals that we have in our firm are working under the leadership of Mr. Pratiraj and fulfilling the needs of the customers as per their requirements.
        </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            Due to the dedication of our team it has become possible for our company to gain huge prominence within shorter duration of time in the markets.
        </Text>
        <Text tag="h4" classes="pad-tb-20">
            Key Facts of Hiranyajay Enterprises Private Limited:
        </Text>
        {companyFeatures.map(({ label, value }, index) => {
            return (
                <Row key={index} classes={index%2 ? '' : 'bg-gray-lighter'}>
                    <Col col={{ xs: 12, sm: 6, md: 6, lg: 4 }} classes="pad-tb-10">
                        <strong>{label}:</strong>
                    </Col>
                    <Col col={{ xs: 12, sm: 6, md: 6, lg: 8 }} classes="pad-tb-10">
                        {value}
                    </Col>
                </Row>
            )
        })}


    </Layout>
)
export default CompanyProfile
