import React from 'react';
import { Text, Layout } from '@base';

const AboutProduct = () => (
    <Layout pad={{ lg: { l: 20, r: 20 }, md: { l: 20, r: 20 } }}>
        <h3>Wide Range of Tested and Efficient Products</h3>
        <Text tag="p" align="justify" classes="pad-t-15">
            The products supplied by us are first tested in our laboratory by our knowledgeable experts who check the proportion of the chemicals used in the production of liquid floor cleaner, floor cleaning chemicals, black phenyl etc.
        </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            Our manufactured items are highly effective and are available in various fragrances and properties from which the user can choose according to his needs. Before selling any product, we make sure it is skin friendly and doesn't causes any adverse reaction or allergy to users skin.
        </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            Our manufactured items are tested at different stages to make sure the product is worth selling and customer may not encounter any issues with these after purchasing.
        </Text>
    </Layout>
)
export default AboutProduct
