import React, { Fragment } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Anchor, Row, Col, Text, Layout } from '@base';


import AboutUs from './AboutUs.jsx';
import AboutProduct from './AboutProduct.jsx';
import OurTeam from './OurTeam.jsx';
import CompanyProfile from './CompanyProfile.jsx';

const AboutMaster = ({
    match: { url }
}) => (
        <Fragment>
            <Row>
                <Col col={{ xs: 12, sm: 12, md: 3, lg: 2 }} classes="pad-b-20">
                    <Text tag="div" size="18" weight="semi-bold" classes="bor-b-gray-light pad-b-10">
                        About Us Topics
                </Text>
                    <Layout flex={{ direction: 'column', justify: 'space-between' }} classes="flex-md-down-row">
                        <Anchor
                            asLink
                            href="/about/detail"
                            noUnderline
                            color="gray"
                            hoverColor="theme"
                            classes="pad-tb-10 bor-b-gray-light"
                            attributes={{
                                activeClassName: 'active-menu',
                            }}
                        >
                            About us
                   </Anchor>
                        <Anchor
                            asLink
                            href="/about/company"
                            noUnderline
                            color="gray"
                            hoverColor="theme"
                            classes="pad-tb-10 bor-b-gray-light"
                            attributes={{
                                activeClassName: 'active-menu',
                            }}
                        >
                            Company profile
                   </Anchor>
                        <Anchor
                            asLink
                            href="/about/product"
                            noUnderline
                            color="gray"
                            hoverColor="theme"
                            classes="pad-tb-10 bor-b-gray-light"
                            attributes={{
                                activeClassName: 'active-menu',
                            }}
                        >
                            About products
                   </Anchor>
                        <Anchor
                            asLink
                            href="/about/team"
                            noUnderline
                            color="gray"
                            hoverColor="theme"
                            classes="pad-tb-10 bor-b-gray-light"
                            attributes={{
                                activeClassName: 'active-menu',
                            }}
                        >
                            Our team
                   </Anchor>
                    </Layout>
                </Col>
                <Col col={{ xs: 12, sm: 12, md: 9, lg: 10 }} classes="pad-b-20">
                    <Switch>
                        <Route exact path={url} render={() => <Redirect to={`${url}/detail`} />} />
                        <Route path={`${url}/detail`} component={AboutUs} />
                        <Route path={`${url}/product`} component={AboutProduct} />
                        <Route path={`${url}/team`} component={OurTeam} />
                        <Route path={`${url}/company`} component={CompanyProfile} />
                        <Route render={() => <Redirect to={`${url}/detail`} />} />
                    </Switch>
                </Col>
            </Row>
        </Fragment>
    )
export default AboutMaster
