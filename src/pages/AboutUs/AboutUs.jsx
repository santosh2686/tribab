import React from 'react';
import { Text, Layout } from '@base';

const AboutUs = () => (
    <Layout pad={{ lg: { l: 20, r: 20 }, md: { l: 20, r: 20 } }}>
        <h3>About Us</h3>
        <Text tag="p" align="justify" classes="pad-t-15">
            Taking in consideration all the aspects like needs of consumers,
            customers choice, affordable rates, high quality ingredients,
            our company named Hiranyajay Enterprises Private Limited started
            its business recently in the year 2018.
            </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            We are one of the noted
            manufacturers, suppliers and traders of products like Black Phenyl,
            Liquid Bleach, Floor Cleaning Chemicals, Liquid Floor Cleaner, etc.
            </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            Based in Navi Mumbai Maharashtra with vast network of distribution,
            we have served customers from different ends. With best infrastructure
            and team of experts, we have made a prominent place in the market by
            supplying effective products.
            </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            From us, a customer will get variety of
            products for different purposes ranging from hand-wash to detergent powders.
            The best part of associating with us is that we give priority to customers
            satisfaction and work in accordance to their demands and instructions.
            </Text>
        <Text tag="p" align="justify" classes="pad-t-15">
            We also maintain honesty in our deals and transparency in our business
            policies, so that the we can have strong and long lasting bonds with our
            customers.
        </Text>
    </Layout>
)
export default AboutUs
