import React, { Fragment } from 'react';
import { MapCssModules } from '@utils';
import { Layout, Row, Col, Text } from '@base';

const Feature = () => (
    <Layout bgColor="gray-lighter" pad="30" classes="home-feature">
        <Row classes="align-center height-100">
            <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} offset={{ xs: 0, sm: 0, md: 6, lg: 6}}>
                <Text tag="h2" color="primary-dark">
                    <Text tag="p">Choose
                    <Text tag="strong" weight="semi-bold">Tribab</Text>
                    </Text>
                    <Text tag="p">
                        and <Text tag="strong" weight="semi-bold">Stay Healthy</Text>
                    </Text>
                </Text>
                <Text tag="p" size="14" align="justify" classes="pad-t-15">
                We  are manufacturing and supplying a wide range of cleaning 
products. Black Phenyl, Liquid Bleach, Floor Cleaning Chemicals, Liquid Floor Cleaner, with vast network of distribution, we have served customers from different ends. With best infrastructure and team of experts, we have made a prominent place in the market by supplying effective products. 
                </Text>
            </Col>
        </Row>
    </Layout>
)
export default MapCssModules(Feature)
