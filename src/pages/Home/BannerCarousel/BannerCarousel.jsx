import React from 'react';
import { MapCssModules } from '@utils';
import { Layout, Row, Col, Text, Button, Image } from '@base';

import promo1 from '@images/promo1.png';
import promo2 from '@images/promo2.png';
import promo3 from '@images/promo3.png';

const BannerCarousel = () => (
    <Layout classes="home-banner">
        <Row classes="height-100 lady-bg">
            <Col col={{ xs: 12, sm: 6, md: 6, lg: 4 }}>
                <Layout
                    flex={{ align: 'center', justify: 'center', direction: 'column' }}
                    pad={{ tb: 20 }}
                    classes="height-100"
                >
                    <Layout>
                        <Text tag="h3" weight="semi-bold">
                            Quality product
                        </Text>
                        <Text tag="h3" weight="bold" classes="pad-b-15">
                            for your Hygienic Care
                        </Text>
                        <Button
                            asLink
                            href="/product"
                            category="theme"
                        >
                            Explore Products
                        </Button>
                    </Layout>
                </Layout>
            </Col>
            <Col col={{ xs: 12, sm: 3, md: 3, lg: 3 }}>
                <Layout
                    flex={{align: 'center', justify: 'space-between'}}
                    classes="flex-sm-up-column"
                >
                    <Layout mar={{ lg: { tb: 10, l: 30 }, md: { tb: 10, l: 30 }, sm: { tb: 10 }, xs: { tb: 10 } }}>
                        <Image src={promo1} classes="width-100" />
                    </Layout>
                    <Layout mar={{ lg: { tb: 10, r: 30 }, md: { tb: 10, r: 30 }, sm: { tb: 10 }, xs: { tb: 10 } }}>
                        <Image src={promo2} classes="width-100" />
                    </Layout>
                    <Layout mar={{ lg: { tb: 10, l: 30 }, md: { tb: 10, l: 30 }, sm: { tb: 10 }, xs: { tb: 10 } }}>
                        <Image src={promo3} classes="width-100" />
                    </Layout>
                </Layout>
            </Col>
        </Row>
    </Layout>
)
export default MapCssModules(BannerCarousel)
