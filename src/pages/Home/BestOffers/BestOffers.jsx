import React, { Fragment } from 'react';
import { MapCssModules } from '@utils';
import { Anchor, Layout, LayoutItem, Text, } from '@base';
import { ProductModule } from '@components';
import carouselData from '@data/carousel.json'
const { banners } = carouselData

const BestOffers = () => (
    <Fragment>
        <Layout flex={{ align: 'center', justify: 'space-between' }} pad={{ t: 30, b: 15 }}>
            <Text size="16" weight="semi-bold" transform="uppercase">Best Seller</Text>
            <Anchor
                asLink
                href="/product"
                noUnderline
                hoverColor="theme"
            >
                View All
            </Anchor>
        </Layout>
        <Layout
            flex={{ align: 'center', justify: 'space-between' }}
            pad={{ b: 30 }}
            classes="overflow-hidden"
            position="relative"
        >
            {banners.map((bannerItem, index) => {
                const { code, category, title, imageUrl } = bannerItem
                return (
                    <LayoutItem classes="slider-item text-center" key={index}>
                        <ProductModule
                            imageUrl={imageUrl}
                            productCategory={category}
                            productName={title}
                            productId={code}
                        />
                    </LayoutItem>
                )
            })}
        </Layout>
    </Fragment>
)
export default MapCssModules(BestOffers)
