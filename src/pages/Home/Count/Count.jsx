import React from 'react';
import { MapCssModules } from '@utils';
import { Row, Col, Layout, Image, Text } from '@base';

import product from '@images/product.png';
import client from '@images/client.png';
import team from '@images/team.png';

const Count = () => (
    <Layout bgColor="primary-dark" color="white">
        <Row>
            <Col col={{ xs: 12, sm: 4, md: 4, lg: 4 }} classes="text-center pad-tb-20">
                <Image src={product} />
                <Text tag="div" size="32" weight="bold" classes="pad-tb-10">
                    20
                    <Text size="18" classes="vertical-top">+</Text>
                </Text>
                <Text tag="div" size="18" weight="semi-bold">Products</Text>
            </Col>
            <Col col={{ xs: 12, sm: 4, md: 4, lg: 4 }} classes="text-center pad-tb-20">
                <Image src={client} />
                <Text tag="div" size="32" weight="bold" classes="pad-tb-10">
                    2000
                <Text size="18" classes="vertical-top">+</Text>
                </Text>
                <Text tag="div" size="18" weight="semi-bold">Happy Clients</Text>
            </Col>
            <Col col={{ xs: 12, sm: 4, md: 4, lg: 4 }} classes="text-center pad-tb-20">
                <Image src={team} />
                <Text tag="div" size="32" weight="bold" classes="pad-tb-10">
                    3
                <Text size="18" classes="vertical-top">+</Text>
                </Text>
                <Text tag="div" size="18" weight="semi-bold">Dealer Networks</Text>
            </Col>
        </Row>
    </Layout>
)

export default MapCssModules(Count)
