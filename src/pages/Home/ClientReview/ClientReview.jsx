import React, { Fragment } from 'react';
import { MapCssModules } from '@utils';
import { Layout, Row, Col, Text } from '@base';

const ClientReview = () => (
    <Layout pad={{ tb: 30 }} classes="client-review">
        <Row classes="align-center height-100">
            <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }}>
                <Text tag="h2" color="primary-dark">
                    <Text tag="p">What our Clients</Text>
                    <Text tag="p">
                        express about <Text tag="strong" weight="semi-bold">Tribab</Text>
                    </Text>
                </Text>
                <Text tag="p" size="14" align="justify" classes="pad-t-15">
                    Always available, extremely best product, and
                    a tremendous focus on hygiene factor. Our company was on strict budget to
                    create a buying for cleaning products and Tribab  exceeded my expectations.
                    The entire company was happy  with it! Without any hesitation… highly
                    recommended products.
               </Text>
                <Text
                    tag="div"
                    color="primary-dark"
                    align="right"
                    size="12"
                    weight="bold"
                    classes="pad-t-10"
                >
                    - Pratik B.
               </Text>
            </Col>
        </Row>
    </Layout>
)
export default MapCssModules(ClientReview)
