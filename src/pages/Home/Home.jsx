import React, { Fragment } from 'react';
import { MapCssModules } from '@utils';

import BannerCarousel from './BannerCarousel/BannerCarousel.jsx';
import BestOffers from './BestOffers/BestOffers.jsx';
import Feature from './Feature/Feature.jsx';
import Count from './Count/Count.jsx';
import ClientReview from './ClientReview/ClientReview.jsx';

const Home = () => (
    <Fragment>
        <BannerCarousel />
        <BestOffers />
        <Feature />
        <Count />
        <ClientReview />
    </Fragment>
)
export default MapCssModules(Home)
