import React, { lazy, Suspense } from 'react';
import { Route, Switch, Redirect } from "react-router-dom";

import { Container, Spinner } from '@base';

const Home = lazy(() => import('../pages/Home/Home.jsx'));
const AboutMaster = lazy(() => import('../pages/AboutUs/AboutMaster.jsx'));
const ProductMaster = lazy(() => import('../pages/Products/ProductMaster.jsx'));
const ProductDetail = lazy(() => import('../pages/ProductDetail/ProductDetail.jsx'));
const ContactMaster = lazy(() => import('../pages/ContactUs/ContactMaster.jsx'));
const Career = lazy(() => import('../pages/Career/Career.jsx'));

const Wrapper = () => (
    <Container classes="flex-1">
        <Suspense fallback={<Spinner />}>
            <Switch>
                <Route exact path='/' render={() => <Redirect to="/home" />} />
                <Route exact path="/home" component={Home} />
                <Route path="/about" component={AboutMaster} />
                <Route path="/product" component={ProductMaster} />
                <Route path="/product-detail/:category/:id" component={ProductDetail} />
                <Route path="/contact" component={ContactMaster} />
                <Route exact path="/career" component={Career} />
                <Route render={() => <Redirect to="/home" />} />
            </Switch>
        </Suspense>
    </Container>
)

export default Wrapper
