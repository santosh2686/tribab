import React from 'react';
import ProtoTypes from 'prop-types'

const Currency = ({
  data
}) => {
  return(
    <span>
      <i className="fa fa-inr color-gray vertical-middle"/>&nbsp;
      {Number(data).toLocaleString('en-IN', {maximumFractionDigits : 2, minimumFractionDigits : 2})}
    </span>)
};

export default Currency;
