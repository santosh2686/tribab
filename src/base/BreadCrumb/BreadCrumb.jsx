import React, { memo } from 'react'
import { NavLink } from 'react-router-dom'
import { arrayOf, object, string } from 'prop-types';
import ClassNames from 'classnames'
import { MapCssModules } from '@utils'

import Anchor from '../Anchor/Anchor.jsx';
import Text from '../Text/Text.jsx';

const BreadCrumb = ({ data, classes }) => {
  const eltClass = ClassNames('breadcrumb', {
    [classes]: classes
  });
  return (
    <div styleName={eltClass}>
      <ul styleName="flex">
        {data.map(({ route, label }, index) => {
          return (<li styleName="relative pad-lr-10" key={index}>
            {route ?
              <Anchor
                href={route}
                color="gray"
                hoverColor="gray-darker"
                asLink
                noUnderline>
                {label}
              </Anchor> :
              <Text color="-gray-darker">{label}</Text>}
          </li>)
        })}
      </ul>
    </div>
  )
};

BreadCrumb.propTypes = {
  data: arrayOf(object),
  classes: string
};

BreadCrumb.defaultProps = {
  data: [{ route: '/', label: 'abc' }, { route: '/', label: 'abc2' }, { label: 'abc3' }],
  classes: ''
};

export default memo(MapCssModules(BreadCrumb))
