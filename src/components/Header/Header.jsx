import React from 'react';
import { MapCssModules } from '@utils';
import { Anchor, Button, Container, Layout, Image } from '@base';
import Navigation from '../Navigation/Navigation.jsx'
import logo from '../../images/tribab_logo.png'

const Header = () => (
  <Container>
    <Layout tag="header" flex={{ align: 'center', wrap: true }} pad={{ t: 5, b: 15 }} position="relative" classes="header">
      <Anchor
        asLink
        href="/home"
      >
        <Image src={logo} name="Tribab logo" classes="mar-r-20" />
      </Anchor>
      <Navigation />
      <div styleName="text-lg-down-center header-button">
        <Button category="theme">
          Become a dealer
        </Button>
        <Button asLink href="/contact/detail" category="theme" outline classes="mar-l-15">Send Enquiry</Button>
      </div>
    </Layout>
  </Container>
);

export default MapCssModules(Header);
