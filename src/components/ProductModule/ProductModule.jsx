import React, { Fragment } from 'react';
import { string } from 'prop-types';
import { MapCssModules } from '@utils';
import { Anchor, Image, Text } from '@base';

const ProductModule = ({ imageUrl, productCategory, productName, productId }) => {
    return (
        <Fragment>
            <Anchor
                asLink
                href={`/product-detail/${productCategory}/${productId}`}
            >
                <Image src={imageUrl} classes="bor-radius-5" />
            </Anchor>
            <Text tag="div" weight="semi-bold" align="center" classes="pad-t-10">
                <Anchor
                    asLink
                    href={`/product-detail/${productCategory}/${productId}`}
                    noUnderline
                    color="gray"
                >
                    {productName}
                </Anchor>
            </Text>
        </Fragment>
    )
}

ProductModule.propTypes = {
    imageUrl: string.isRequired,
    productName: string.isRequired,
    productId:  string.isRequired,
}

export default MapCssModules(ProductModule)
