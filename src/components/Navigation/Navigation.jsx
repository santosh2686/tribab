import React, { Fragment, PureComponent } from 'react';
import { withRouter } from "react-router-dom";
import classNames from 'classnames';
import { Layout, Anchor } from '@base';
import Hamburger from './Hamburger.jsx';

class Navigation extends PureComponent {
    state = {
        show: false
    }

    componentDidUpdate(prevProps) {
        const { location } = this.props;
        if (location !== prevProps.location) {
            this.setState({ show: false })
        }
    }

    toggleNavigation = () => {
        this.setState((prevProps) => ({ show: !prevProps.show }))
    }

    render() {
        const { show } = this.state;
        const eltClass = classNames('flex-1 navigation-menu', {
            'active': show
        })
        return (
            <Fragment>
                <Layout tag="ul" flex={{ align: 'center' }} classes={eltClass}>
                    <Anchor
                        asLink
                        href="/home"
                        noUnderline
                        classes="mar-r-20 font-semi-bold"
                        hoverColor="theme"
                        attributes={{
                            activeClassName: 'active-menu',
                        }}
                    >
                        HOME
                    </Anchor>
                    <Anchor
                        asLink
                        href="/about"
                        noUnderline
                        classes="mar-r-20 font-semi-bold"
                        hoverColor="theme"
                        attributes={{
                            activeClassName: 'active-menu',
                        }}
                    >
                        ABOUT US
                    </Anchor>
                    <Anchor
                        asLink
                        href="/product"
                        noUnderline
                        classes="mar-r-20 font-semi-bold"
                        hoverColor="theme"
                        attributes={{
                            activeClassName: 'active-menu',
                        }}
                    >
                        PRODUCT
                    </Anchor>
                    <Anchor
                        asLink
                        href="/contact"
                        noUnderline
                        classes="mar-r-20 font-semi-bold"
                        hoverColor="theme"
                        attributes={{
                            activeClassName: 'active-menu',
                        }}
                    >
                        CONTACT
                </Anchor>
                </Layout>
                <Hamburger
                    show={show}
                    toggleHandler={this.toggleNavigation}
                />
            </Fragment>
        );
    }
}


export default withRouter(Navigation);
