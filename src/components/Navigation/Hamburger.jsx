import React from 'react';
import classNames from 'classnames';
import { MapCssModules } from '@utils';
import { Button } from '@base';

const Hamburger = ({ show, toggleHandler }) => {
    const eltClass = classNames('responsive-nav-handle pad-0 flex-lg-down-unset relative', {
        'active': show
    })
    return (
        <Button
            category="plain"
            classes={eltClass}
            clickHandler={() => toggleHandler(!show)}
        >
            <span styleName="nav-handle-inner" />
        </Button>
    )
}

export default MapCssModules(Hamburger)
