import React from 'react';
import { MapCssModules } from '@utils';
import { Anchor, Row, Col, Image } from '@base';
import youTube from '@images/youTube.png';
import facebook from '@images/facebook.png';
import twitter from '@images/twitter.png';
import instagram from '@images/instagram.png';

const FooterTop = () => (
    <Row classes="align-center text-center">
        <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="text-md-up-left pad-b-20">
            <Anchor
                asLink
                href="/career"
                noUnderline
                hoverColor="theme"
                classes="mar-r-20"
            >
                Career
        </Anchor>
            <Anchor
                asLink
                href="/about/company"
                noUnderline
                hoverColor="theme"
                classes="mar-r-20"
            >
                Company profile
        </Anchor>
            <Anchor
                asLink
                href="/contact/faq"
                noUnderline
                hoverColor="theme"
            >
                Faq
        </Anchor>
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 6, lg: 6 }} classes="text-md-up-right pad-b-20">
            <Anchor href="/" classes="pad-r-5">
                <Image src={youTube} />
            </Anchor>
            <Anchor
                href="https://www.facebook.com/Tribab-108590364004054/"
                classes="pad-r-5"
                attributes={{
                    rel: "nofollow",
                    target: "_blank"
                }}
            >
                <Image src={facebook} />
            </Anchor>
            <Anchor href="/" classes="pad-r-5">
                <Image src={twitter} />
            </Anchor>
            <Anchor href="/" classes="pad-r-5">
                <Image src={instagram} />
            </Anchor>
        </Col>
    </Row>
);

export default MapCssModules(FooterTop);
