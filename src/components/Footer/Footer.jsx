import React from 'react';
import { MapCssModules } from '@utils';
import { Container, Layout } from '@base';

import FooterTop from './FooterTop.jsx';
import FooterBottom from './FooterBottom.jsx';

const Footer = () => (
  <Layout
    bgColor="gray-lighter"
    pad={{ tb: 20 }}
  >
    <Container>
      <FooterTop />
      <FooterBottom />
    </Container>
  </Layout>
);

export default MapCssModules(Footer);
