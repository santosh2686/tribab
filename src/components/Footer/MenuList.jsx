import React from 'react';
import { arrayOf, string } from 'prop-types';
import { MapCssModules } from '@utils';
import { Anchor, Layout } from '@base';

const MenuList = ({ list }) => (
    <Layout
        tag="ul"
    >
        {list.map((listItem, index) => {
            return (
                <li key={index} styleName="pad-t-10">
                    <Anchor href="/" color="gray" noUnderline>
                        {listItem}
                    </Anchor>
                </li>
            )
        })}
    </Layout>
);

MenuList.propTypes = {
    list: arrayOf(string)
}

MenuList.defaultProps = {
    list: []
}

export default MapCssModules(MenuList);
