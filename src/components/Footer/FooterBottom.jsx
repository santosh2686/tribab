import React from 'react';
import { MapCssModules } from '@utils';
import { Anchor, Row, Col, Text, Layout } from '@base';
import MenuList from './MenuList.jsx';

const FooterBottom = () => (
    <Row>
        <Col col={{ xs: 12, sm: 12, md: 6, lg: 2 }} classes="pad-b-20">
            <Text weight="semi-bold">Quick Links</Text>
            <ul>
                <li styleName="pad-t-10">
                    <Anchor
                        asLink href="/product/dish-wash"
                        color="gray"
                        noUnderline
                    >
                        Dish wash
                    </Anchor>
                </li>
                <li styleName="pad-t-10">
                    <Anchor
                        asLink href="/product/floor-cleaner"
                        color="gray"
                        noUnderline
                    >
                        Floor cleaner
                    </Anchor>
                </li>
                <li styleName="pad-t-10">
                    <Anchor
                        asLink href="/product/green-phenyl"
                        color="gray"
                        noUnderline
                    >
                        Green phenyl
                    </Anchor>
                </li>
            </ul>
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 6, lg: 2 }} classes="pad-b-20">
            <Text weight="semi-bold" classes="hidden-md-down">&nbsp;</Text>
            <ul>
                <li styleName="pad-t-10">
                    <Anchor
                        asLink href="/product/hand-cleaner"
                        color="gray"
                        noUnderline
                    >
                        Hand cleaner
                    </Anchor>
                </li>
                <li styleName="pad-t-10">
                    <Anchor
                        asLink href="/product/liquid-bleach"
                        color="gray"
                        noUnderline
                    >
                        Liquid bleach
                    </Anchor>
                </li>
            </ul>
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 6, lg: 2 }} classes="pad-b-20">
            <Text weight="semi-bold" classes="hidden-md-down">&nbsp;</Text>
            <ul>
                <li styleName="pad-t-10">
                    <Anchor
                        asLink href="/product/milky-phenyl"
                        color="gray"
                        noUnderline
                    >
                        Milky phenyl
                    </Anchor>
                </li>
                <li styleName="pad-t-10">
                    <Anchor
                        asLink href="/product/toilet-cleaner"
                        color="gray"
                        noUnderline
                    >
                        Toilet cleaner
                    </Anchor>
                </li>
            </ul>
        </Col>
        <Col col={{ xs: 12, sm: 12, md: 6, lg: 4 }} offset={{ xs: 0, sm: 0, md: 0, lg: 2 }} classes="pad-b-20">
            <Text weight="semi-bold">Hiranyajay Enterprises Private Limited</Text>
            <Text tag="p" classes="pad-t-10">
                D-Magniferra, 403, Casabella Gold, Kalyan-Shil Road,
            </Text>
            <Text tag="p">
                Dombivali - 421204, Maharashtra, India
            </Text>
            <Layout pad={{ t: 15 }}>
                <Text>Email: </Text>
                <Anchor href="mailto:connect@tribab.com">
                    connect@tribab.com
                </Anchor>
            </Layout>
        </Col>
    </Row>
);

export default MapCssModules(FooterBottom);
