import React from 'react';
import { MapCssModules } from '@utils';
import classNames from 'classnames';

const SpriteIcon = ({ classes }) => {
    const eltClass = classNames('image-sprite', {
        [classes]: classes
    })
    return (
        <div styleName={eltClass}>data</div>
    )
}

export default MapCssModules(SpriteIcon)
