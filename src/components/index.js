import Header from './Header/Header.jsx';
import Footer from './Footer/Footer.jsx';
import ProductModule from './ProductModule/ProductModule.jsx';
import ScrollToTop from './ScrollToTop/ScrollToTop.jsx';

export {
    Header,
    Footer,
    ProductModule,
    ScrollToTop,
}
