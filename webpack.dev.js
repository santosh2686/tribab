const path = require('path');

module.exports = {
  entry: ['babel-polyfill', './index.js'],
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'bundle.js',
    publicPath: '/build/'
  },
  devServer: {
    port: process.env.PORT || 9090,
    host: '0.0.0.0',
    historyApiFallback: true,
    compress: true,
  },
  resolve: {
    extensions: ['.jsx', '.js', '.json'],
    alias: {
      '@base': path.resolve(__dirname, './src/base'),
      '@utils': path.resolve(__dirname, './src/utils'),
      '@components': path.resolve(__dirname, './src/components'),
      '@config': path.resolve(__dirname, './src/config'),
      '@images': path.resolve(__dirname, './src/images'),
      '@data': path.resolve(__dirname, './src/product-data')
    }
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, {
      test: /\.s?css$/,
      use: [{
        loader: 'style-loader',
      },{
        loader: 'css-loader',
        options: {
          modules: {
            localIdentName: '[local]'
          },
          importLoaders: 1,
        }
      },{
        loader: 'sass-loader'
      }]
    }, {
      test: /\.(png|svg|jpg|gif)$/,
      loader: 'file-loader',
      options: {
        name: 'images/[name].[ext]?[hash]'
      }
    }]
  }
};
